#!/usr/bin/python3
# Simple show IP address to the LCD panel
# Author: Martin Zeleny <martin.zeleny@volny.cz>

import RPi_I2C_driver
import subprocess

def main():
    lcd = RPi_I2C_driver.lcd()
    ip = subprocess.check_output(["hostname", "-I"]).decode()
    lcd.lcd_display_string(ip, 1)

if __name__ == "__main__":
    main()

