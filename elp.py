#!/usr/bin/python3
#
# Electronic Panel
#
# Shows departures from bus stop to LCD with 20x4 characters connected via I2C
# Author: Martin Zeleny <martin.zeleny@volny.cz>

import RPi_I2C_driver
import time
import requests
import unicodedata
import json
import logging


class elp(RPi_I2C_driver.lcd):
    """ Electronic Panel encapsulation
        Shows information directly to LCD
    """
    req                = 'http://iris.bmhd.cz/api/getDeparturesNew.php'
    param              = {'stopId': '1443'}
    LCD_WIDTH          = 20
    LCD_BOTTOM_ROW_NUM = 4
    lcd                = RPi_I2C_driver.lcd()
    printTime          = True

    logging.basicConfig(filename='elp.log', filemode='w', format='%(asctime)s %(levelname)s %(message)s')

    def inicializeLCD(self):
        """ Create custom chars a write them into the LCD """
        plusminus = [
            0b00100,
            0b00100,
            0b11111,
            0b00100,
            0b00100,
            0b00000,
            0b11111,
            0b00000
        ]
        fontdata = [
            plusminus, #accessible as chr(0)
            [0] * 8,   #clear rest of custom characters
            [0] * 8,
            [0] * 8,
            [0] * 8,
            [0] * 8,
            [0] * 8,
            [0] * 8
        ]
        self.lcd.lcd_load_custom_chars(fontdata)


    def removeDia(self, text):
        """ Remove diacritics from utf8 text """
        text = unicodedata.normalize('NFKD', text)

        output = ''
        for c in text:
            if not unicodedata.combining(c):
                output += c

        return output


    def showTime(self):
        """ Print custom formatted date and time to the bottom line of the display """
        t = time.strftime('%a   %d.%m.   %H:%M') # 'Tue   22.05.   19:14'
        self.lcd_display_string(t, self.LCD_BOTTOM_ROW_NUM)


    def solveException(self, msg, e, r=None):
        logging.error(msg)
        logging.error(e)

        if r:
            logging.error(r)
            logging.error(r.text)

        self.lcd_clear()
        self.lcd_display_string(msg[:self.LCD_WIDTH].center(self.LCD_WIDTH), 1)
        self.showTime()


    def showDepartures(self, jsonRes):
        """ Obtain information about departures in json format as a parameter
            Write directly to LCD
        """
        rowNum = 1
        for sign in jsonRes['PostList']: #iterate over stop signs
            if sign['Name'] == 'město':
                for dep in sign['Departures']:
                    lineNum = '{:>3s}'.format(dep['Line'])
                    endStop = '{:10s}'.format(self.removeDia(dep['EndStop'][:10]))

                    depTime = dep['Time']
                    if depTime[0] == '±':              #e.i '± 21:30'
                        depTimeList = list(depTime[1:])
                        depTimeList[0] = chr(0)        #write char 0
                                                       #custom representation of '±'
                        depTime = ''.join(depTimeList) #result: '±21:30'
                    depTime = '{:>6s}'.format(depTime)

                    row = lineNum + ' ' + endStop + depTime
                    self.lcd_display_string(row, rowNum)

                    if rowNum == self.LCD_BOTTOM_ROW_NUM - 1 and self.printTime:
                        self.showTime()
                        break

                    if (rowNum == len(sign['Departures'])) and (rowNum < self.LCD_BOTTOM_ROW_NUM):
                        self.showTime()
                        break

                    rowNum += 1
                    if rowNum > self.LCD_BOTTOM_ROW_NUM:
                        break


    def run(self):
        while True:
            try:
                r = requests.get(self.req, self.param, timeout=5)
                j = json.loads(r.text, strict=False)
                self.showDepartures(j)
                self.printTime = not self.printTime #invert flag for printing date and time

            except requests.exceptions.Timeout as e:
                self.solveException("Timeout", e)
            except requests.exceptions.RequestException as e:
                self.solveException("RequestException", e)
            except ValueError as e:
                self.solveException("ValueError", e, r)
            except OSError as e:
                self.solveException("OSError", e, j)
                print(j)
                print("====== QUIT ======")
                break
            time.sleep(5)


def main():
    self = elp()
    self.inicializeLCD()
    self.run()


if __name__ == "__main__":
    main()

