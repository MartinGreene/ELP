#!/usr/bin/python3
#
# Simple write departures of tram and bus to stdout.
# Author: Martin Zeleny <martin.zeleny@volny.cz>

import requests
from fake_useragent import UserAgent

def main():
    URL = 'https://mapa.idsjmk.cz/api/departures'
    param = {'stopid': '1659'}

    header = {'User-Agent': str(UserAgent().random)}
    r = requests.get(URL, params=param, headers=header, timeout=10)

    try:
        for departures in r.json()['PostList']:
            for vehicle in departures['Departures']:
                print(vehicle['LineName'], vehicle['TimeMark'])
            print()
    except requests.exceptions.JSONDecodeError as e:
        print(e)
        print()
        print(r.text)


if __name__ == "__main__":
    main()

