#!/usr/bin/python3
#
# Demo
#
# Prints characters to LCD with 20x4 connected via I2C
# Author: Martin Zeleny <martin.zeleny@volny.cz>

import RPi_I2C_driver
import time

def main():
    myLCD = RPi_I2C_driver.lcd()
    i = 0

    while True:
        for row in range(1, 5):
            for col in range(20):
                myLCD.lcd_display_string_pos(chr(i), row, col)
                i += 1
                time.sleep(0.2)
        myLCD.lcd_write(RPi_I2C_driver.LCD_RETURNHOME)

if __name__ == "__main__":
    main()

